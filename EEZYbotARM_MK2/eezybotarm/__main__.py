import argparse
import sys

from autobahn.twisted.component import Component, run
import txaio
from txaio.interfaces import log_levels

from eezybotarm.controller import Controller
from eezybotarm.interfaces import Topics

txaio.use_twisted()


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    parser = argparse.ArgumentParser()
    parser.add_argument('--usb-device', default='/dev/ttyACM0')
    parser.add_argument('--realm', default='realm')
    parser.add_argument('--topic', default='it.eezybotarm.mk2')
    parser.add_argument('--url', default='ws://localhost:8080/ws')
    parser.add_argument('--loglevel', default='info', choices=log_levels)
    parser.add_argument('--debug', dest='loglevel', action='store_const',
                        const='debug')
    args = parser.parse_args(argv)
    txaio.start_logging(level=args.loglevel)
    topics = Topics.with_root(args.topic)
    app = Controller(args.usb_device, topics)
    component = Component(transports=args.url, realm=args.realm)
    app.component = component
    component.on('join', app.joined)
    run([component])


if __name__ == "__main__":
    main()

