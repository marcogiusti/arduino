from dataclasses import asdict
from typing import List

from autobahn.wamp.exception import ApplicationError
from twisted.application.internet import backoffPolicy
from twisted.internet.defer import inlineCallbacks
from twisted.internet.serialport import SerialPort
from twisted.protocols.basic import LineOnlyReceiver
import txaio

from eezybotarm.interfaces import PartialState, State, Topics


class EEZYbotArmProtocol(LineOnlyReceiver):

    log = txaio.make_logger()

    def connectionMade(self):
        self.controller.connectionMade()

    def connectionLost(self, reason):
        self.controller.connectionLost(reason)

    def lineReceived(self, line: bytes):
        self.log.debug('<< {line}', line=line)
        line_s = line.decode('ascii')
        command, *args = line_s.split()
        f = getattr(self, f'cmd_{command}', None)
        if f is not None:
            f(args)

    def sendLine(self, line):
        self.log.debug('>> {line}', line=line)
        super().sendLine(line)

    def cmd_POS(self, args: List[str]):
        position = State(*map(int, args))
        self.controller.publish_position(asdict(position))

    def get_position(self):
        self.sendLine(b'GET_POS')

    def set_position(self, position: PartialState):
        cmd = (f'SET_POS {position.servo0} {position.servo1} '
               f'{position.servo2} {position.servo3}')
        self.sendLine(cmd.encode('ascii'))


class EEZYbotArmController:

    log = txaio.make_logger()
    protocol = None
    _failedAttempts = 0

    def __init__(self, session, topics, port, retryPolicy=None, reactor=None):
        self.session = session
        self.topics = topics
        self.port = port
        self._timeoutForAttempt = retryPolicy
        if reactor is None:
            from twisted.internet import reactor
        self.reactor = reactor

    def connect(self):
        self.log.debug("connecting")
        protocol = EEZYbotArmProtocol()
        protocol.controller = self
        try:
            SerialPort(protocol, self.port, self.reactor, timeout=5)
            self.protocol = protocol
        except BaseException as exc:
            if exc.__context__:
                failure = txaio.create_failure(exc.__context__)
            else:
                failure = txaio.create_failure(exc)
            self.log.error(txaio.failure_message(failure))
            if self._timeoutForAttempt is not None:
                self._failedAttempts += 1
                delay = self._timeoutForAttempt(self._failedAttempts)
                self.log.debug(
                    "Scheduling retry {attempt} to connect {port} "
                    "in {delay} seconds.",
                    attempt=self._failedAttempts,
                    port=self.port,
                    delay=delay,
                )
                self._retryCall = self.reactor.callLater(delay, self.connect)

    def stopRetrying(self):
        self._retryCall.cancel()
        del self._retryCall

    def connectionMade(self):
        self.log.debug("connection made")

    def connectionLost(self, reason):
        self.log.debug("connection lost: {reason}",
                       reason=txaio.failure_message(reason))
        self.protocol = None
        self._failedAttempts = 0
        self.connect()

    def get_position(self):
        if self.protocol is None:
            raise ApplicationError(self.topics.serial_disconnected)
        self.protocol.get_position()

    def set_position(self, new_position):
        if self.protocol is None:
            raise ApplicationError(self.topics.serial_disconnected)
        self.protocol.set_position(PartialState(**new_position))

    def publish_position(self, position):
        self.session.publish(self.topics.position, position)


class Controller:

    def __init__(self, usb_device, topics):
        self.usb_port = usb_device
        self.topics = topics

    @inlineCallbacks
    def joined(self, session, details):
        retryPolicy = backoffPolicy()
        controller = EEZYbotArmController(session, self.topics, self.usb_port,
                                          retryPolicy)
        yield session.register(controller.get_position, self.topics.position)
        yield session.register(controller.set_position, self.topics.set_position)
        controller.connect()
