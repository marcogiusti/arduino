from dataclasses import dataclass
from typing import Union


@dataclass
class State:
    servo0: int
    servo1: int
    servo2: int
    servo3: int


@dataclass
class PartialState:
    servo0: Union[int, str] = '-'
    servo1: Union[int, str] = '-'
    servo2: Union[int, str] = '-'
    servo3: Union[int, str] = '-'


@dataclass
class Topics:

    root: str
    position: str
    set_position: str
    serial_disconnected: str

    @classmethod
    def with_root(cls, topic_root='it.eezybotarm.mk2'):
        return cls(topic_root,
                   f'{topic_root}.position',
                   f'{topic_root}.set_position',
                   f'{topic_root}.error.serial_disconnected')
