try {
    var autobahn = require('autobahn');
} catch (e) {
    // when running in browser, AutobahnJS will
    // be included without a module system
}


function call_later(func, delay, args, cancel_token) {
    var timeoutID;
    var done = false;

    const promise = new Promise((resolve, reject) => {

        if (cancel_token) {
            cancel_token.promise.then(function onCancelled(reason) {
                if (!done) {
                    clearTimeout(timeoutID);
                    done = true;
                    reject(reason);
                }
            });
        }

        timeoutID = setTimeout(() => {
            done = true;
            try {
                const retvalue = func(...args);
                resolve(retvalue);
            } catch(err) {
                reject(err);
            }
        }, delay * 1000);
    });

    return promise;
}


function try_call(session, topic, args, conf) {
    const _conf = {
        max_delay: 300,
        initial_delay: 1.5,
        factor: 1.5,
        jitter: 0.1,
        ...conf
    };
    var delay = _conf.initial_delay;

    function retry_call(err) {
        if (_conf.jitter) {
            delay = autobahn.util.rand_normal(delay, delay * _conf.jitter);
        }
        delay = Math.min(delay * _conf.factor, _conf.max_delay);
        session.log(`retry calling ${topic} in ${delay} s`);
        return call_later(() => session.call(topic, args), delay, [], _conf.cancel_token)
            .catch(_err => {
                if (isCancel(_err)) {
                    return _err;
                } else {
                    return retry_call(_err);
                }
            });
    }

    session.log(`calling ${topic}`);
    return session.call(topic, args).catch(retry_call);
}


class EEZYbotArmTopics {
    constructor(root) {
        this.root = root;
        this.position = `${root}.position`;
        this.set_position = `${root}.set_position`;
    }
}

function set_servo_value(state, name, value) {
    const { robot_arm } = state;

    $(`#${name}_lbl`).text(`${value}`);
    $(`#${name}`).val(value);
    robot_arm[name] = value;
}


class Application {
    constructor(topics) {
        this.topics = topics;
        this.cancel_get_position = null;
    }

    start(session, detail) {
        const { token: cancel_token, cancel } = CancelToken.source();
        const state = {
            session,
            topics: this.topics,
            robot_arm: {
                servo0: -1,
                servo1: -1,
                servo2: -1,
                servo3: -1,
            }
        }
        this.cancel_get_position = cancel;
        this.session = session;

        session.subscribe(this.topics.position, args => this.on_position_changed(state, args));
        try_call(session, this.topics.position, [], { cancel_token }).then(
            console.log, console.err);
        $("input[name*='servo']").on("change",
            state,
            this.do_set_position);
    }

    stop(reason, details) {
        this.session = null;
        if (this.cancel_get_position) {
            const cancel = this.cancel_get_position;
            this.cancel_get_position = null;
            cancel("Connection closed");
        }
        $("input[name*='servo']").off("change", this.do_set_position);
    }

    do_set_position(event) {
        const { session, topics, robot_arm } = event.data;
        const payload = {};
        const range = $(this);
        const servo_name = this.name;
        const servo_value = this.value;
        payload[servo_name] = servo_value;
        session.call(topics.set_position, [payload]).then(
            () => {
                set_servo_value(state, servo_name, servo_value);
            },
            () => { range.val(robot_arm[servo_name]); },
        );
    }

    on_position_changed(state, args){
        const [position] = args;
        const { robot_arm } = state;
        for (const [name, value] of Object.entries(position)) {
            set_servo_value(state, name, value);
        }
    }
}

const topics = new EEZYbotArmTopics('it.eezybotarm.mk2');
const app = new Application(topics);
const connection = new autobahn.Connection({
    url: 'ws://192.168.178.69:8080/ws',
    realm: 'realm',
    max_retries: -1,
});
connection.onopen = (session, details) => app.start(session, details);
connection.onclose = (reason, details) => app.stop(reason, details);
connection.open();
