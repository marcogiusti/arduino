#include <Adafruit_PWMServoDriver.h>

#include "ServoDriver.h"

int usToTicks(const int pulse_width_us, const int servo_freq);

void ServoDriver::rotate(int angle) {
  int pulse_width;

  angle = min(angle, servo.max_angle);
  angle = max(angle, servo.min_angle);
  pulse_width = map(angle, servo.min_angle, servo.max_angle,
                    servo.min_pulse_width, servo.max_pulse_width);
  rotate_pulse_width(pulse_width);
}

void ServoDriver::rotate_pulse_width(int pulse_width) {
  pulse_width = min(pulse_width, servo.max_pulse_width);
  pulse_width = max(pulse_width, servo.min_pulse_width);
  driver->setPWM(channel, 0, usToTicks(pulse_width, servo.freq));
  current_pulse_width = pulse_width;
}

int ServoDriver::get_pulse_width() { return current_pulse_width; }

int ServoDriver::get_position() {
  int angle = map(current_pulse_width, servo.min_pulse_width,
                  servo.max_pulse_width, servo.min_angle, servo.max_angle);
  return angle;
}

int usToTicks(const int pulse_width_us, const int servo_freq) {
  int pulse_width_ticks;
  const uint32_t usPerSec = 1000000;
  const int tickMin = 0;
  const int tickMax = 4095;

  pulse_width_ticks =
      map(pulse_width_us, 0, usPerSec / servo_freq, tickMin, tickMax);
  return pulse_width_ticks;
}
