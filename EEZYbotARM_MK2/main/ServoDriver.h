#ifndef _servodriver_h
#define _servodriver_h

#include <Adafruit_PWMServoDriver.h>

#include "ServoDriver.h"

struct ServoDesc {
  int min_angle;
  int max_angle;
  int min_pulse_width;
  int max_pulse_width;
  int freq;
};

class ServoDriver {
  Adafruit_PWMServoDriver *driver;
  uint8_t channel;
  struct ServoDesc servo;
  uint16_t current_pulse_width;

public:
  ServoDriver();
  ServoDriver(Adafruit_PWMServoDriver *driver, uint8_t channel,
              struct ServoDesc servo)
      : driver(driver), channel(channel), servo(servo),
        current_pulse_width(0){};
  void rotate(int angle);
  void rotate_pulse_width(int pulse_width);
  int get_position();
  int get_pulse_width();
};

#endif
