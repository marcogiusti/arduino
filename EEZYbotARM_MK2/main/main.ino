#include <errno.h>
#include <limits.h>
#include <stdio.h>

#include <Adafruit_PWMServoDriver.h>
#include <Arduino.h>
#include <SerialCommands.h>

#include "ServoDriver.h"

#define SERVO0_CHANNEL 0
#define SERVO1_CHANNEL 1
#define SERVO2_CHANNEL 2
#define SERVO3_CHANNEL 3
#define PCS9685_FREQ 50
#define PCA9685_OSC_FREQ 25900000

#define NO_SERVOS 4

int parse_positive_int(char *str);
void write_servo_position(Stream *serial, int servo_no);
void write_position(Stream *serial);
void cmd_unrecognized(SerialCommands *sender, const char *cmd);
void hello(SerialCommands *sender);
void get_pos(SerialCommands *sender);
void set_pos(SerialCommands *sender);

Adafruit_PWMServoDriver driver = Adafruit_PWMServoDriver();
const struct ServoDesc MG995 = {
    .min_angle = 0,
    .max_angle = 180,
    .min_pulse_width = 350,
    .max_pulse_width = 2500,
    .freq = 50,
};
const struct ServoDesc FS90 = {
    .min_angle = 0,
    .max_angle = 120,
    .min_pulse_width = 900,
    .max_pulse_width = 2100,
    .freq = 50,
};
// Configure the servos.
ServoDriver servos[NO_SERVOS] = {
    ServoDriver(&driver, SERVO0_CHANNEL, MG995),
    ServoDriver(&driver, SERVO1_CHANNEL, MG995),
    ServoDriver(&driver, SERVO2_CHANNEL, MG995),
    ServoDriver(&driver, SERVO3_CHANNEL, FS90),
};

// Serial commands buffer
char cmd_buf[32];
SerialCommands serial_commands(&Serial, cmd_buf, sizeof(cmd_buf));
SerialCommand cmd_hello("HELLO", hello);
SerialCommand cmd_get_pos("GET_POS", get_pos);
SerialCommand cmd_set_pos("SET_POS", set_pos);

void setup() {
  // Setup the servo driver
  driver.begin();
  driver.setOscillatorFrequency(PCA9685_OSC_FREQ);
  driver.setPWMFreq(PCS9685_FREQ);
  // Setup the Serial
  Serial.begin(9600);
  // Setup the serial commands
  serial_commands.AddCommand(&cmd_hello);
  serial_commands.AddCommand(&cmd_get_pos);
  serial_commands.AddCommand(&cmd_set_pos);
  serial_commands.SetDefaultHandler(&cmd_unrecognized);
  // Set initial position
  servos[0].rotate(90);
  servos[1].rotate(90);
  servos[2].rotate(90);
  servos[3].rotate(60);
  delay(10);
}

void loop() {
  serial_commands.ReadSerial();
  delay(15);
}

int parse_positive_int(char *str) {
  char *endptr;
  long val;

  errno = 0;
  val = strtol(str, &endptr, 10);
  if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) ||
      (errno != 0 && val == 0)) {
    return -1;
  }
  // Empty string
  if (endptr == str) {
    return -1;
  }
  // out of range
  if (val < 0 || val > INT_MAX) {
    return -1;
  }
  return val;
}

void cmd_unrecognized(SerialCommands *sender, const char *cmd) {
  sender->GetSerial()->print("ERROR: Unrecognized command [");
  sender->GetSerial()->print(cmd);
  sender->GetSerial()->println("]");
}

void hello(SerialCommands *sender) { Serial.println("hallo"); }

void write_position(Stream *serial) {
  serial->print("POS");
  for (int servo_no = 0; (unsigned)servo_no < NO_SERVOS; servo_no++) {
    serial->print(" ");
    serial->print(servos[servo_no].get_position());
  }
  serial->println("");
}

void get_pos(SerialCommands *sender) {
#ifdef DEBUG
  sender->GetSerial()->println("LOG got GET_POS command");
#endif
  write_position(sender->GetSerial());
}

void set_pos(SerialCommands *sender) {
  int positions[NO_SERVOS];

#ifdef DEBUG
  sender->GetSerial()->println("LOG got SET_POS command");
#endif

  for (int servo_no = 0; (unsigned)servo_no < NO_SERVOS; servo_no++) {
    char *arg;
    int position;

    arg = sender->Next();
    if (arg[0] == '-' && arg[1] == '\0') {
      position = -1;
    } else {
      position = parse_positive_int(arg);
      if (position < 0) {
        return;
      }
    }
    positions[servo_no] = position;
  }

  for (int servo_no = 0; (unsigned)servo_no < NO_SERVOS; servo_no++) {
    int position = positions[servo_no];
    if (position >= 0) {
      servos[servo_no].rotate(position);
    }
  }

  write_position(sender->GetSerial());
}
