==========================
The HC-05 bluetooth module
==========================

.. image:: https://i.creativecommons.org/l/by-sa/4.0/80x15.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Creative Commons Attribution-ShareAlike 4.0 International License.

Projects
========

Some projects to learn to use the HC-05 bluetooth module.

1. `HC-05 AT commands`_
2. `Controlling LEDs`_

.. _HC-05 AT commands: projects/01-at-commands
.. _Controlling LEDs: projects/02-leds

Internal references
===================

PDFs that I saved locally to avoid loosing them if the websites go
offline. For this material I did not found any copyright notice, I
suppose they are in the public domain.

| `User manual`_
| `HC-05 datasheet`_

..
   The module HC-05 has 6 pins: `Vcc`, `Gnd`, `Txd`, `Rxd`, `Key` and
   `State`. `Vcc` and `Gnd` are well known. `Txd` and `Rdx` are the pins
   used for the communication. The pin `Txd` of the bluetooth module is
   connected to the `Rxd` pin of the Arduino, and the `Txd` pin of the
   Arduino is connected to the `Rxd` pin of the bluetooth module. `Key` is
   the pin used to control the operation mode of the module and `State` is
   an output pin

.. _User manual: docs/dsh.772-148.1.pdf
.. _HC-05 datasheet: docs/istd016A.pdf

.. vim: set tw=72
