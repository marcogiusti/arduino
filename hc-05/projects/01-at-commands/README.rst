=================
HC-05 AT commands
=================

.. image:: https://i.creativecommons.org/l/by-sa/4.0/80x15.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Creative Commons Attribution-ShareAlike 4.0 International License.

.. TODO add an entry in the glossary for the AT mode

This sketch sends some `AT commands`_ to the bluetooth module to show
how to use the AT operation mode. The communication looks like this::

   >> AT
   OK
   >> AT+VERSION?
   +VERSION:2.0-20100601
   OK
   >> AT+ADDR?
   +ADDR:2014:5:261043
   OK
   >> AT+NAME?
   +NAME:H-C-2010-06-01
   OK
   ...

The operation mode of the bluetooth module is controlled by the *Key*
pin: when *LOW* the module in in the communication mode, when *HIGH* it
is in the AT mode. In this sketch I show the difference between wiring
the *Key* pin to *Vcc* and controlling the operation mode with Arduino.
In a next sketch I will show a different way to control the operation
mode of the module.

For this project I used an Arduino Mega 2560 because I use two serial
ports, one to communicate with the bluetooth module and the second to
communicate with my computer to send log of the communication.

.. TODO add a link to the pin in the doc
.. TODO add a link to the sketch
.. TODO add a link to the documentation for the AT commands

Controlling *Key* pin with Arduino
==================================

*Key* pin connected to the pin 7 of the Arduino.

.. image:: at-commands-1_bb_25.png
   :align: center
   :target: at-commands-1_bb.png

The log of the communication is avaialble in the file
`communication-1.txt`_.  When running the sketch in this configuration,
the command `STATE` at the end reports the state as *PAIRABLE*::

   >> AT+STATE?
   +STATE:PAIRABLE
   OK


Wiring *Key* pin to *Vcc*
=========================

In this case I did not bother to change the code because not necessary.
I just rewired the *Key* pin.

.. image:: at-commands-2_bb_25.png
   :align: center
   :target: at-commands-2_bb.png

The log of the communication is avaialble in the file
`communication-2.txt`_. Differently from the previous configuration the
`STATE` command reports the state as *INITIALIZED*::

   >> AT+STATE?
   +STATE:INITIALIZED
   OK

Internal references
===================

| `HC-05 datasheet`_ with the list of supported AT commands.

External references
===================

| https://www.teachmemicro.com/hc-05-bluetooth-command-list
| https://maker.pro/custom/tutorial/hc-05-bluetooth-transceiver-module-datasheet-highlights

.. _communication-1.txt: ./communication-1.txt
.. _communication-2.txt: ./communication-2.txt
.. _AT commands: https://en.wikipedia.org/wiki/Hayes_command_set
.. _HC-05 datasheet: ../../docs/istd016A.pdf
