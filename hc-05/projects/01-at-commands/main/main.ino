void send_at_command(String command);

const int BT_KEY_PIN = 7;

void setup(void) {
    /* Set up the serial port to log the communication */
    Serial.begin(9600);
    /* Set the port for the bluetooth module */
    Serial1.begin(38400);

    /* Set the key pin high to enable the AT mode in the device */
    pinMode(BT_KEY_PIN, OUTPUT);
    digitalWrite(BT_KEY_PIN, HIGH);

    /* Send the commands to the module */
    send_at_command("AT");
    send_at_command("AT+VERSION?");
    send_at_command("AT+ADDR?");
    send_at_command("AT+NAME?");
    send_at_command("AT+ROLE?");
    send_at_command("AT+CLASS?");
    send_at_command("AT+IAC");
    send_at_command("AT+INQM?");
    send_at_command("AT+PSWD?");
    send_at_command("AT+UART?");
    send_at_command("AT+CMODE?");
    send_at_command("AT+BIND?");
    send_at_command("AT+POLAR?");
    send_at_command("AT+IPSCAN?");
    send_at_command("AT+SNIFF?");
    send_at_command("AT+SENM?");
    send_at_command("AT+ADCN?");
    send_at_command("AT+MRAD?");
    send_at_command("AT+STATE?");
}

void loop(void) {}

void send_at_command(String command) {
    /* Write the command in the serial monitor */
    Serial.print(">> ");
    Serial.println(command);
    /* Send the command to the bluetooth module */
    Serial1.println(command);
    /* Receive the response from the bluetooth module */
    String response = Serial1.readString();
    Serial.print(response);
}
