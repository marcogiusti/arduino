================
Controlling LEDs
================

..
   ___/\  /\  /\  ___
        \/  \/  \/

.. image:: https://i.creativecommons.org/l/by-sa/4.0/80x15.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Creative Commons Attribution-ShareAlike 4.0 International License.

A simple project to show how to use the HC-05 bluetooth module to
receive data from an Android phone: Arduino waits for fom commands from
the bluetooth device an turn on/turn off the respective LED.

To notice how the pin *TxD* of the bluetooth module is connected to the
*RxD* pin of the Arduino, and the *TxD* pin of the Arduino is connected
to the *RxD* pin of the bluetooth module. The *Key* pin is connected to
*Ground* to set the module in communication mode.

::

      HC-05         Arduino

   +---------+    +---------+
   |         |    |         |
   |     TxD +----+ RxD     |
   |         |    |         |
   |     RxD +----+ TxD     |
   |         |    |         |
   |     Gnd +----+ Gnd     |
   |         |    |         |
   +---------+    +---------+

The specific Android app I used to communicate with the bluetooth module
is called "Arduino bluetooth controller" [1]_. Not any specific reason to
use this application over any other you can find, just I find it easy to
set up, the interface is clean and it has four operation modes:

- Controller mode
- Switch mode
- Dimmer mode
- Terminal mode

I personally set the app in Controller mode and configured the buttons
as following:

| Square: **R** (toggle the yellow LED)
| Cross: **G** (toggle the green LED)
| Circle: **Y** (toggle the red LED)

The LEDs can also be controlled from the terminal mode, being able to
change more values at once.

.. image:: select_mode_25.jpg
   :target: select_mode.jpg

.. image:: controller_25.jpg
   :target: controller.jpg

.. image:: configuration_25.jpg
   :target: configuration.jpg

.. image:: terminal_25.jpg
   :target: terminal.jpg

Schematics
==========

.. image:: bt-leds_bb_25.png
   :align: center
   :target: bt-leds_bb.png

.. [1] https://play.google.com/store/apps/details?id=com.giumig.apps.bluetoothserialmonitor&hl=en_US&gl=US

.. vim: set tw=72
