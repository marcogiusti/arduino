/* vim: set tw=72 */

/*
   Simple project to control three LEDs from the bluetooth.

   With this sketch you see how to use Arduino with the HC-05 bluetooth
   module. To transmit to the bluetooth module I used the "Arduino
   bluetooth controller" [1] Android app in Controller mode, with the
   following configuration:

   Square: R
   Cross: Y
   Circle: G

   Commands are, for ease of implementation, only one byte long.
   They toggle the LEDs connected to Arduino.

   [1] https://play.google.com/store/apps/details?id=com.giumig.apps.bluetoothserialmonitor&hl=en_US&gl=US
*/

/* LED pins */
const int YELLOW_LED_PIN = 3;
const int GREEN_LED_PIN = 4;
const int RED_LED_PIN = 5;

/* Indexes in the states array */
const int YELLOW_IDX = 0;
const int GREEN_IDX = 1;
const int RED_IDX = 2;

/*
   Allocate the space for the states and defined the initial state for
   each LED.
*/
int states[3] = {LOW, LOW, LOW};

void setup() {
    pinMode(YELLOW_LED_PIN, OUTPUT);
    pinMode(GREEN_LED_PIN, OUTPUT);
    pinMode(RED_LED_PIN, OUTPUT);
    digitalWrite(YELLOW_LED_PIN, LOW);
    digitalWrite(GREEN_LED_PIN, LOW);
    digitalWrite(RED_LED_PIN, LOW);

    /*
       Setup the bluetooth device.

       By default, if in communication mode, the baud rate is 38400. If
       the communication does not work, try using different speeds like
       9600.
     */
    Serial.begin(38400);
    Serial.setTimeout(1000);
}

void swap_state(int index, int pin) {
    /*
       The next line can be rewritten like this to be more explicit:

        const int prevState = states[index];
        int nexState;
        if (prevState == HIGH) {
            nexState = LOW;
        } else {
            nexState = HIGH;
        }
        states[index] = nexState;
    */
    states[index] = ++states[index] % 2;
    digitalWrite(pin, states[index]);
}

void loop() {
    if (Serial.available()) {
        const char cmd = Serial.read();
        switch (cmd) {
            case 'G':
                swap_state(GREEN_IDX, GREEN_LED_PIN);
                break;
            case 'Y':
                swap_state(YELLOW_IDX, YELLOW_LED_PIN);
                break;
            case 'R':
                swap_state(RED_IDX, RED_LED_PIN);
                break;
        }
    }
}
