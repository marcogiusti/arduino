#include "MinecraftOreLamp.h"
#include "colours.h"

void MinecraftOreLamp::setup() {
  pixels.begin();
  _set_colour(Colour::Black);
}

void MinecraftOreLamp::_set_colour(uint32_t colour) {
  for (uint16_t i = 0; i < num_leds; i++) {
    pixels.setPixelColor(i, colour);
  }
  pixels.show();
}

void MinecraftOreLamp::set_colour(uint32_t colour) {
  last_colour = colour;
  _set_colour(colour);
  if (smartnest_report_client != NULL) {
    smartnest_report_client->report_colour(colour);
  }
}

void MinecraftOreLamp::set_brightness(uint8_t brightness) {
  brightness = map(brightness, 0, 100, 0, 255);
  pixels.setBrightness(brightness);
  _set_colour(last_colour);
  if (smartnest_report_client != NULL) {
    smartnest_report_client->report_brightness(brightness);
  }
}

void MinecraftOreLamp::power_on() {
  if (last_colour == 0xFFFFFFFF) {
    _set_colour(Colour::White);
  } else {
    _set_colour(last_colour);
  }
}

void MinecraftOreLamp::power_off() { _set_colour(Colour::Black); }
