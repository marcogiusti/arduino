#ifndef __DIAMOND_ORE_LAMP_H
#define __DIAMOND_ORE_LAMP_H

#include "SmartnestReportClient.h";
#include <Adafruit_NeoPixel.h>

class MinecraftOreLamp {
  Adafruit_NeoPixel pixels;
  uint16_t num_leds;
  uint32_t last_colour;

  /**
   * If smartnest_report_client is not NULL, every time a property is set, it
   * is also reported to Smartnest servers.
   */
  SmartnestReportClient *smartnest_report_client;

  /**
   * Set the colour of the light and tell the controller to show it.
   */
  void _set_colour(uint32_t colour);

public:
  MinecraftOreLamp(uint16_t n, uint16_t pin, SmartnestReportClient *smartnest_report_client)
      : pixels(n, pin, NEO_GRB + NEO_KHZ800), num_leds(n),
        last_colour(0xFFFFFFFF), smartnest_report_client(smartnest_report_client) {}

  /**
   * One time setup function for the lamp.
   */
  void setup();

  /**
   * Set the colour of the light and tell the controller to show it.
   * Also remember the colour used, to better handle te brightness.
   */
  void set_colour(uint32_t colour);

  /**
   * Set the colour of the lamp using one value for each component.
   */
  void set_colour(uint8_t red, uint8_t green, uint8_t blue) {
    set_colour((uint32_t)red << 16 | (uint32_t)green << 8 | blue);
  }

  /**
   * Set the brightness of the light, if supported. Range is 0-100.
   */
  void set_brightness(uint8_t brightness);

  /**
   * Power on the light. Set by default the colour to white.
   */
  void power_on();

  /**
   * Power off the light.
   */
  void power_off();
};

#endif
