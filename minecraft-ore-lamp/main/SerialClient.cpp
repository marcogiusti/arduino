#include "SerialClient.h"

void SerialClient::repl_ok(char *seq) {
  serial_commands.GetSerial()->print("OK ");
  serial_commands.GetSerial()->println(seq);
}

void SerialClient::repl_nok(char *seq) {
  serial_commands.GetSerial()->print("NOK ");
  serial_commands.GetSerial()->println(seq);
}

void SerialClient::cmd_set_colour() {
  char *seq = serial_commands.Next();
  char *arg = serial_commands.Next();
  char *endptr;
  uint32_t colour;

#ifdef DEBUG
  serial_commands.GetSerial()->print(">> SET_COLOUR ");
  serial_commands.GetSerial()->print(seq);
  serial_commands.GetSerial()->print(" ");
  serial_commands.GetSerial()->println(arg);
#endif
  colour = strtoul(arg, &endptr, 0);
  if (*endptr == '\0') {
    lamp.set_colour(colour);
    repl_ok(seq);
  } else {
    repl_nok(seq);
  }
}

void SerialClient::cmd_set_brightness() {
  char *seq = serial_commands.Next();
  char *arg = serial_commands.Next();
  char *endptr;
  unsigned long brightness;

#ifdef DEBUG
  serial_commands.GetSerial()->print(">> SET_COLOUR ");
  serial_commands.GetSerial()->print(seq);
  serial_commands.GetSerial()->print(" ");
  serial_commands.GetSerial()->println(arg);
#endif
  brightness = strtoul(arg, &endptr, 0);
  if (*endptr == '\0') {
    uint8_t b = min(brightness, (unsigned long)100);
    b = map(b, 0, 100, 0, 255);
    lamp.set_brightness(b);
    repl_ok(seq);
  } else {
    repl_nok(seq);
  }
}

void SerialClient::default_handler(const char *cmd) {
  serial_commands.GetSerial()->print("ERROR: Unrecognized command [ ");
  serial_commands.GetSerial()->print(cmd);
  serial_commands.GetSerial()->println(" ]");
}

void SerialClient::loop() { serial_commands.ReadSerial(); }

void SerialClient::add_command(SerialCommand *command) {
  serial_commands.AddCommand(command);
}

void SerialClient::set_default_handler(void (*function)(SerialCommands *,
                                                        const char *)) {
  serial_commands.SetDefaultHandler(function);
}
