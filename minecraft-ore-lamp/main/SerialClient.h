#ifndef __SERIAL_CLIENT_H
#define __SERIAL_CLIENT_H

#include "MinecraftOreLamp.h";
#include <SerialCommands.h>

class SerialClient {
  char cmd_buf[32];
  SerialCommands serial_commands;
  MinecraftOreLamp &lamp;

  /**
   * Send an OK response with the sequence number of the command.
   */
  void repl_ok(char *seq);

  /**
   * Send an NOK response with the sequence number of the command.
   */
  void repl_nok(char *seq);

public:
  SerialClient(Stream &serial, MinecraftOreLamp &lamp)
      : serial_commands(&serial, cmd_buf, sizeof(cmd_buf), "\n"), lamp(lamp) {}
  void add_command(SerialCommand *command);
  void set_default_handler(void (*function)(SerialCommands *, const char *));
  void loop();

  /**
   * Set the colour of the lamp.
   *
   * Format:
   *	SET_COLOUR <seq> <colour>
   *
   * seq: the command sequence number.
   * colour: the chosen colour.
   */
  void cmd_set_colour();

  /**
   * Set the brightness of the light.
   *
   * Format:
   *	SET_BRIGHTNESS <seq> <percentage>
   *
   * seq: the command sequence number.
   * percentage: the percentage of brightness, between 0 and 100.
   */
  void cmd_set_brightness();
  void default_handler(const char *cmd);
};

#endif
