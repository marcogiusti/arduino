#include "SmartnestClient.h";
#include <stdio.h>
#include <string>

#define SMARTNEST_CLIENT_DEBUG

void SmartnestClient::debug(const char *str, boolean new_line) {
#ifdef SMARTNEST_CLIENT_DEBUG
  if (new_line) {
    Serial.println(str);
  } else {
    Serial.print(str);
  }
#endif
}

void SmartnestClient::debug(int i, boolean new_line) {
  debug(std::to_string(i).c_str(), new_line);
}

void SmartnestClient::loop() {
  if (!mqtt_client.connected()) {
    unsigned int m = millis();
    if (last_reconnect_attempt + reconnect_delay < m) {
      last_reconnect_attempt = m;
      _connect();
    }
  } else {
    mqtt_client.loop();
  }
}

void SmartnestClient::_connect() {
  debug("Connecting to MQTT...");

  if (mqtt_client.connect(this->client_id, this->username, this->password)) {
    debug("connected");
  } else {
    if (mqtt_client.state() == 5) {
      debug("Connection not allowed by broker, possible reasons:");
      debug("- Device is already online. Wait some seconds until it appears "
            "offline for the broker");
      debug("- Wrong Username or password. Check credentials");
      debug("- Client Id does not belong to this username, verify ClientId");

    } else {
      debug("Not possible to connect to Broker Error code:");
      debug(mqtt_client.state());
    }
  }
}

void SmartnestClient::connect(const char *client_id, const char *username, const char *password) {
  char topic[100];

  this->client_id = client_id;
  this->username = username;
  this->password = password;
  while (!mqtt_client.connected()) {
    _connect();
    if (!mqtt_client.connected()) {
      delay(0x7530);
    }
  }
  last_reconnect_attempt = millis();
  snprintf(topic, 100, "%s/#", client_id);
  mqtt_client.subscribe(topic); // Subscribes to all messages send to the device
}

void SmartnestClient::message_received(char *topic, const uint8_t *payload,
                                       unsigned int length) {
  char *action;
  char message[length + 1];
  memcpy(message, payload, length);
  message[length] = '\0';

  debug(">> ", false);
  debug(topic, false);
  debug(" ", false);
  debug(message);

  get_action(topic, &action);
  if (action == NULL) {
    // pass
  } else if (strcmp(action, "color") == 0) {
    msg_color(payload, length);
  } else if (strcmp(action, "powerState") == 0) {
    msg_power_state(payload, length);
  } else if (strcmp(action, "percentage") == 0) {
    msg_percentage(payload, length);
  } else {
    debug("Unknown action");
  }
}

void SmartnestClient::get_action(char *topic, char **action) {
  char *received_client_id;
  char *directive;

  *action = NULL;
  received_client_id = strtok(topic, "/");
  if (strcmp(received_client_id, client_id) == 0) {
    directive = strtok(NULL, "/");
    if (strcmp(directive, "directive") == 0) {
      *action = strtok(NULL, "\n");
    }
  }
}

void SmartnestClient::msg_color(const uint8_t *payload, unsigned int length) {
  char message[length + 1];
  unsigned long red = 0, green = 0, blue = 0;
  int n;

  memcpy(message, payload, length);
  message[length] = '\0';
  n = sscanf(message, "rgb(%u,%u,%u)", &red, &green, &blue);
  if (n == 3) {
    debug("set colour to ", false);
    debug(message);
    lamp.set_colour(red, green, blue);
  }
}

void SmartnestClient::msg_power_state(const uint8_t *payload,
                                      unsigned int length) {
  char message[length + 1];

  memcpy(message, payload, length);
  message[length] = '\0';
  debug("power state: ", false);
  debug(message);
  if (strcmp(message, "ON") == 0) {
    lamp.power_on();
  } else {
    lamp.power_off();
  }
}

void SmartnestClient::msg_percentage(const uint8_t *payload,
                                     unsigned int length) {
  char message[length + 1];
  char *endptr;
  uint32_t brightness;

  memcpy(message, payload, length);
  message[length] = '\0';
  debug("set brightness: ", false);
  debug(message);
  brightness = strtoul(message, &endptr, 0);
  if (*endptr == '\0') {
    brightness = min(brightness, (uint32_t)100);
    lamp.set_brightness(brightness);
  }
}
