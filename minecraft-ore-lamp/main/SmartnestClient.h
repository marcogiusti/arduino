#ifndef __SMARTNEST_CLIENT_H
#define __SMARTNEST_CLIENT_H

class MinecraftOreLamp;

#include "MinecraftOreLamp.h";
#include <PubSubClient.h>

class SmartnestClient {
  MinecraftOreLamp &lamp;
  PubSubClient &mqtt_client;
  const char *client_id;
  const char *username;
  const char *password;
  unsigned int last_reconnect_attempt;
  unsigned int reconnect_delay = 30000;

  void debug(const char *str, boolean new_line = true);
  void debug(int i, boolean new_line = true);

  void _connect();
  void get_action(char *topic, char **action);
  void msg_color(const uint8_t *payload, unsigned int length);
  void msg_power_state(const uint8_t *payload, unsigned int length);
  void msg_percentage(const uint8_t *payload, unsigned int length);

public:
  SmartnestClient(MinecraftOreLamp &lamp, PubSubClient &mqtt_client)
      : lamp(lamp), mqtt_client(mqtt_client), client_id(NULL), username(NULL),
        password(NULL), last_reconnect_attempt(0) {}
  void connect(const char *client_id, const char *username, const char *password);
  void loop();
  void message_received(char *topic, const uint8_t *payload,
                        unsigned int length);
};

#endif
