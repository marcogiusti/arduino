#include "SmartnestReportClient.h";
#include <stdio.h>

#define SMARTNEST_REPORT_CLIENT_DEBUG

void SmartnestReportClient::debug(const char *str, boolean new_line) {
#ifdef SMARTNEST_REPORT_CLIENT_DEBUG
  if (new_line) {
    Serial.println(str);
  } else {
    Serial.print(str);
  }
#endif
}

void SmartnestReportClient::report(const char *action, const char *message) {
  char topic[100];

  snprintf(topic, 100, "%s/report/%s", client_id, action);
#ifdef SMARTNEST_REPORT_CLIENT_DEBUG
  debug("<< ", false);
  debug(topic, false);
  debug(" ", false);
  debug(message);
#endif
  mqtt_client.publish(topic, message);
}

void SmartnestReportClient::report_colour(uint32_t colour) {
  char message[] = "rgb(xxx,xxx,xxx)";
  uint8_t red = colour >> 16 & 0xFF;
  uint8_t green = colour >> 8 & 0xFF;
  uint8_t blue = colour & 0xFF;

  snprintf(message, sizeof message, "rgb(%u,%u,%u)", red, green, blue);
  report("color", message);
}

void SmartnestReportClient::report_brightness(uint8_t brightness) {
  /* report("percentage") */
}
