#ifndef __SMARTNEST_REPORT_CLIENT_H
#define __SMARTNEST_REPORT_CLIENT_H

#include <PubSubClient.h>

class SmartnestReportClient {
  PubSubClient &mqtt_client;
  const char *client_id;

  void debug(const char *str, boolean new_line = true);

public:
  SmartnestReportClient(PubSubClient &mqtt_client, const char *client_id)
      : mqtt_client(mqtt_client), client_id(client_id) {}
  void report(const char *topic, const char *message);
  void report_colour(uint32_t colour);
  void report_brightness(uint8_t brightness);
};

#endif /* __SMARTNEST_REPORT_CLIENT_H */
