#include "MinecraftOreLamp.h"
#include "SerialClient.h"
#include "SmartnestClient.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SerialCommands.h>
#include <stdio.h>

void cmd_set_colour(SerialCommands *sender);
void cmd_set_brightness(SerialCommands *sender);
void cmd_clear(SerialCommands *sender);

const uint16_t NUM_LEDS = 5;
const uint16_t PIN = 5;
const char *FIRMWARE_VERSION = "0.0.1";
const char *SSID_NAME = "xxx";
const char *WIFI_PASSWORD = "xxx";
/* IPAddress MQTT_SERVER(3, 122, 209, 170); */
const IPAddress MQTT_SERVER(192, 168, 10, 194);
const uint16_t MQTT_PORT = 1883;
const char *MQTT_CLIENT_ID = "xxx";
const char *MQTT_USERNAME = "xxx";
const char *MQTT_PASSWORD = "xxx";

// MQTT
WiFiClient wifi_client;
PubSubClient mqtt_client(wifi_client);
// Smartnest report
SmartnestReportClient smartnest_report_client(mqtt_client, MQTT_CLIENT_ID);
// Minecraft Ore Lamp
MinecraftOreLamp lamp(NUM_LEDS, PIN, &smartnest_report_client);
// Smartnest
SmartnestClient smartnest_client(lamp, mqtt_client);
// Serial commands
SerialClient serial_client(Serial, lamp);
SerialCommand cmd_set_colour_("SET_COLOUR", cmd_set_colour);
SerialCommand cmd_set_brightness_("SET_BRIGHTNESS", cmd_set_brightness);

void cmd_set_colour(SerialCommands *sender) { serial_client.cmd_set_colour(); }

void cmd_set_brightness(SerialCommands *sender) {
  serial_client.cmd_set_brightness();
}

void default_handler(SerialCommands *sender, const char *cmd) {
  serial_client.default_handler(cmd);
}

void setupSerial() {
  serial_client.add_command(&cmd_set_colour_);
  serial_client.add_command(&cmd_set_brightness_);
  serial_client.set_default_handler(&default_handler);
}

void setupWiFi(const char *ssid_name, const char *wifi_password) {
  WiFi.begin(SSID_NAME, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
  WiFi.printDiag(Serial);
}

void callback(char *topic, byte *payload, unsigned int length) {
  smartnest_client.message_received(topic, payload, length);
}

void setupMqtt(IPAddress server, uint16_t port) {
  mqtt_client.setServer(server, port);
  mqtt_client.setCallback(callback);
}

void setup() {
  char signal[5];

  Serial.begin(115200);
  Serial.println();
  lamp.setup();
  setupSerial();
  setupWiFi(SSID_NAME, WIFI_PASSWORD);
  setupMqtt(MQTT_SERVER, MQTT_PORT);
  smartnest_client.connect(MQTT_CLIENT_ID, MQTT_USERNAME, MQTT_PASSWORD);

  snprintf(signal, 5, "%d", WiFi.RSSI());
  // Reports that the device is online
  smartnest_report_client.report("online", "true");
  // Reports the firmware version
  smartnest_report_client.report("firmware", FIRMWARE_VERSION);
  // Reports the ip
  smartnest_report_client.report("ip", WiFi.localIP().toString().c_str());
  // Reports the network name
  smartnest_report_client.report("network", WiFi.SSID().c_str());
  // Reports the signal strength
  smartnest_report_client.report("signal", signal);
  // Reports the signal strength
  /* smartnest_report_client.report("color", signal); */
}

void loop() {
  serial_client.loop();
  smartnest_client.loop();
  delay(30);
}
