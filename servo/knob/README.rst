Controlling a servo position using a potentiometer
==================================================

The code is based on the `knob example`_ in the `Arduino IDE`_.

.. _knob example: https://github.com/arduino/ArduinoCore-arc32/blob/master/libraries/Servo/examples/Knob/Knob.ino
.. _Arduino IDE: https://www.arduino.cc/en/software

Servo specifications
--------------------

The specifications we need to correctly configure the software are the
operating angle and the pulse width range. These values are reported in
the source code as constants. The minimum and maximum pulse width are
passed to the ``attach()`` method and also used to scale the input value
to a value that is in between the range of accepted values by the servo.
The operating angle is not used but it would be needed if we want to
calculate the angle from the input value.

The FS90 servo
--------------

This is the micro servo used for this example. The datasheet_ from
Pololu_ reports the following specifications:

| Operating angle: 120°
| Pulse width range: 900 ~ 2100 µs

.. _datasheet: https://www.pololu.com/file/0J1435/FS90-specs.pdf
.. _Pololu: https://www.pololu.com/product/2818

Measurements
------------

| ``Vcc = 4.88``
| ``T = 20 ms``
| ``f = 50 Hz``
| ``τ  = Vin * (SERVO_MAX_PULSE_WIDTH - SERVO_MIN_PULSE_WIDTH) / Vcc + SERVO_MIN_PULSE_WIDTH``
| ``δ = τ / T * 100``

+--------------+-------------+-----------+
| ``Vin`` [V]  | ``τ`` [ms]  | ``δ`` [%] |
+==============+=============+===========+
| 0.002        | 0.90        | 4.5       |
+--------------+-------------+-----------+
| 1.010        | 1.14        | 5.7       |
+--------------+-------------+-----------+
| 2.014        | 1.39        | 7         |
+--------------+-------------+-----------+
| 3.014        | 1.66        | 8.3       |
+--------------+-------------+-----------+
| 4.01         | 1.88        | 9.4       |
+--------------+-------------+-----------+
| 4.88         | 2.10        | 10.5      |
+--------------+-------------+-----------+

.. figure:: pulse_width_0V.png

   The pulse width when ``Vin = 0 V``

.. figure:: pulse_width_4_88V.png

   The pulse width when ``Vin = 4.88 V``

A note about Servo.write()
--------------------------

The method Servo.write is meant for servos that operate between 0° and
180° only. If the servo is operating in a smaller range, like the FS90,
or a bigger reange, some servos operate between 0° ~ 270°, this function
is not apt for a correct positioning. A better function is reported
below::

   void writeAngle(int angle) {
       int pulse_width;

       if (angle < SERVO_MIN_ANGLE) {
           angle = SERVO_MIN_ANGLE;
       }
       if (angle > SERVO_MAX_ANGLE) {
           angle = SERVO_MAX_ANGLE;
       }
       pulse_width = map(angle, SERVO_MIN_ANGLE, SERVO_MAX_ANGLE,
               SERVO_MIN_PULSE_WIDTH, SERVO_MAX_PULSE_WIDTH);
       servo.writeMicroseconds(pulse_width);
   }
