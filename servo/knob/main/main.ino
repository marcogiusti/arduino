// Controlling a servo position using a potentiometer (variable resistor)
// by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

#include <Servo.h>

#define SERVO_PIN 9
#define POTENTIOMETER_PIN 0
#define INPUT_MIN_VALUE 0
#define INPUT_MAX_VALUE 1023
#define SERVO_MIN_ANGLE 0
#define SERVO_MAX_ANGLE 120
#define SERVO_MIN_PULSE_WIDTH 900
#define SERVO_MAX_PULSE_WIDTH 2100

Servo servo;  // create servo object to control a servo

void setup()
{
    // attaches the servo on pin 9 to the servo object
    servo.attach(SERVO_PIN, SERVO_MIN_PULSE_WIDTH, SERVO_MAX_PULSE_WIDTH);
}

void loop()
{
    // variable to read the value from the analog pin
    int val;
    int pulse_width;

    // reads the value of the potentiometer (value between 0 and 1023)
    val = analogRead(POTENTIOMETER_PIN);
    // calculate the pulse width from the input
    pulse_width = map(val, INPUT_MIN_VALUE, INPUT_MAX_VALUE,
            SERVO_MIN_PULSE_WIDTH, SERVO_MAX_PULSE_WIDTH);
    // sets the servo position according to the scaled value
    servo.writeMicroseconds(pulse_width);
    // waits for the servo to get there
    delay(15);
}
