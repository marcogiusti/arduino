Controlling a servo position with the Adafruit Servo Shield
===========================================================

Follow up of the knob_ example using the `Adafruit servo shield`_ for
Arduino. The shield uses the I\ :sup:`2`\ C-bus controlled `PCA9685
controller`_.  The shield has the advantage to use only the SDA and SCL
pins on the Arduino, allowing up 16 PWM signals per shield and up to 64
signals when stacking more shields.

There are two steps more required when using the `Adafruit PWM Servo
Driver Library`_ that are:

1. calibrate the PCA9685 internal oscillator, and for this an
   oscilloscope is required;
2. setting the frequency of the signal.

.. _knob: ../knob
.. _Adafruit servo shield: https://learn.adafruit.com/adafruit-16-channel-pwm-slash-servo-shield
.. _PCA9685 controller: https://cdn-shop.adafruit.com/datasheets/PCA9685.pdf
.. _Adafruit PWM Servo Driver Library: https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library

Measurements
------------

| ``Vcc = 4.86``
| ``T = 19.92 ms``
| ``f = 50.2 Hz``

+--------------+-------------+-----------+
| ``Vin`` [V]  | ``τ`` [ms]  | ``δ`` [%] |
+==============+=============+===========+
| 0.009        | 0.895       | 4.49      |
+--------------+-------------+-----------+
| 1.008        | 1.13        | 5.67      |
+--------------+-------------+-----------+
| 2.004        | 1.38        | 6.93      |
+--------------+-------------+-----------+
| 3.001        | 1.63        | 8.18      |
+--------------+-------------+-----------+
| 4.01         | 1.88        | 9.44      |
+--------------+-------------+-----------+
| 4.86         | 2.09        | 10.49     |
+--------------+-------------+-----------+

.. figure:: pulse_width_0V.png

   The pulse width when ``Vin = 0 V``. Offset is 19.92 ms, that I found
   aligning the second pulse with the time axis (y). The period is not
   precise.
