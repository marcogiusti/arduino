#include <Adafruit_PWMServoDriver.h>

#define POTENTIOMETER_PIN 8
#define INPUT_MIN_VALUE 0
#define INPUT_MAX_VALUE 1023
#define PCA9685_OSC_FREQ 25900000
#define SERVO_NUM 0
#define SERVO_FREQ 50
#define SERVO_MIN_ANGLE 0
#define SERVO_MAX_ANGLE 120
#define SERVO_MIN_PULSE_WIDTH 900
#define SERVO_MAX_PULSE_WIDTH 2100

int usToTicks(int pulse_width_us);

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

void setup()
{
    pwm.begin();
    pwm.setOscillatorFrequency(PCA9685_OSC_FREQ);
    pwm.setPWMFreq(SERVO_FREQ);
    delay(10);
}

void loop()
{
    // variable to read the value from the analog pin
    int val;
    int pulse_width;

    // reads the value of the potentiometer (value between 0 and 1023)
    val = analogRead(POTENTIOMETER_PIN);
    // calculate the pulse width from the input
    pulse_width = map(val, INPUT_MIN_VALUE, INPUT_MAX_VALUE,
            SERVO_MIN_PULSE_WIDTH, SERVO_MAX_PULSE_WIDTH);
    // sets the servo position according to the scaled value
    pwm.setPWM(SERVO_NUM, 0, usToTicks(pulse_width));
    // waits for the servo to get there
    delay(15);
}

int usToTicks(int pulse_width_us) {
    int pulse_width_ticks;
    const uint32_t usPerSec = 1000000;
    const int tickMin = 0;
    const int tickMax = 4095;

    pulse_width_ticks = map(pulse_width_us, 0, usPerSec / SERVO_FREQ, tickMin,
            tickMax);
    return pulse_width_ticks;
}
